import React from 'react';
import { Header, Router } from './components';

function App() {
  return (
    <div className="App">
      <Header />
      <div className="site-content">
        <Router />
      </div>
    </div>
  );
}

export default App;
