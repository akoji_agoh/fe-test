import React from 'react';
import PropType from 'prop-types';
import FilterItem from './FilterItem';

const Filter = ({ categories, activeCategories, onSelect, onClearAll }) => {
    const isCategoryActive = (category) => {
        if (!activeCategories || activeCategories.length <= 0) {
            return false;
        }
        const categoryFound = activeCategories.find(current => current.title === category.title) || null;
        return categoryFound !== null;
    };

    return (
        <div className="category-filter">
            <label>Filter by categories</label>
            <ul>
                <FilterItem
                    title="All Categories"
                    isActive={!activeCategories || activeCategories.length <= 0}
                    onSelect={() => onClearAll()}
                />
                {categories.map((category, index) => 
                    <FilterItem
                        key={index}
                        title={category.title}
                        isActive={isCategoryActive(category)}
                        onSelect={() => onSelect(category)}
                    />
                )}
            </ul>
        </div>
    );
};

Filter.propTypes = {
    categories: PropType.array.isRequired,
    activeCategories: PropType.any.isRequired,
    onSelect: PropType.func.isRequired,
    onClearAll: PropType.func.isRequired,
};

export default Filter;
