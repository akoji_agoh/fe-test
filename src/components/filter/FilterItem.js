import React from 'react';
import PropTypes from 'prop-types'

const FilterItem = ({ title, isActive, onSelect }) => (
    <li
        className={isActive ? 'active' : ''}
        onClick={() => onSelect()}
    >
        {title}
        <span />
    </li>
);

FilterItem.propTypes = {
    title: PropTypes.string.isRequired,
    isActive: PropTypes.bool.isRequired,
    onSelect: PropTypes.func.isRequired,
};

export default FilterItem;
