import React from 'react';
import { shallow } from 'enzyme';
import Filter from '../Filter';

describe('Filter component unit test', () => {
    const mockSelectCallBack = jest.fn();
    const mockClearAllCallBack = jest.fn();
    const props = {
        categories: [
            { title: 'Category 1', data: [] },
            { title: 'Category 2', data: [] },
            { title: 'Category 3', data: [] },
        ],
        activeCategories: [],
        onSelect: mockSelectCallBack,
        onClearAll: mockClearAllCallBack,
    };
    let component = null;

    beforeEach(() => {
        component = shallow(<Filter { ...props } />);
    });

    it('should render filter component', () => {
        expect(component).toHaveLength(1);
        expect(component).toMatchSnapshot();
    });
});
