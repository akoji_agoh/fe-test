import React from 'react';
import { shallow } from 'enzyme';
import FilterItem from '../FilterItem';

describe('FilterItem component unit test', () => {
    const mockOnSelect = jest.fn();
    const props = {
        title: 'Category 1',
        isActive: false,
        onSelect: mockOnSelect,
    };

    let component = null;

    beforeEach(() => {
        component = shallow(<FilterItem { ...props } />);
    });

    it('should render FilterItem component', () => {
        expect(component).toHaveLength(1);
        expect(component).toMatchSnapshot();
    });

    it('should display given title', () => {
        expect(component.find('li').html()).toContain(props.title);
    });

    it('should display filter item as active when isActive is true', () => {
        component.setProps({ isActive: true });
        expect(component.find('li').hasClass('active')).toBeTruthy();
    });
    
    it('should detect toggle of onSelect', () => {
        component.find('li').props().onClick();
        expect(mockOnSelect).toHaveBeenCalledTimes(1);
    });
});
