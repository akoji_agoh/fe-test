import React from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import PropType from 'prop-types';
import { FaShoppingBasket } from 'react-icons/fa';
import { getTotalItemsInCart } from '../../settings';
import Logo from '../../assets/img/Logo.svg';

const Header = ({ shoppingCart }) => {    

    return (
        <header>
            <div className="container">
                <NavLink to="/" className="logo">
                    <img src={Logo} alt="Logo" />
                </NavLink>
                <ul className="nav-list">
                    <li>
                        <NavLink to="/">Our Products</NavLink>
                    </li>
                    <li>
                        <NavLink to="/shopping-cart">
                            <FaShoppingBasket className="basket-icon" />
                            <span className="cart-total">{getTotalItemsInCart(shoppingCart)}</span>
                        </NavLink>
                    </li>
                </ul>
            </div>
        </header>
    );
};

Header.propTypes = {
    shoppingCart: PropType.object,
};

const mapStateToProps = (state) => {
    return {
        shoppingCart: state.shoppingCart,
    };
};

export default connect(mapStateToProps, null)(Header);
