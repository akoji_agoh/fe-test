import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { BrowserRouter as Router } from 'react-router-dom';
import Header from '../Header';

describe('Header Test', () => {
    let header = null;

    beforeAll(() => {
        header = (
            <Router>
                <Header />
            </Router>
        );
    });

    it('should render as expected', () => {
        const { asFragment } = render(header);
        expect(asFragment()).toMatchSnapshot();
    });

    it('should contain a logo', () => {
        render(header);
        expect(screen.getByText('Logo')).toBeInTheDocument();
    });
});
