import React from 'react';
import PropType from 'prop-types';

const Hero = ({ backgroundImage, title, text }) => (
    <div
        className="hero-wrapper"
        style={{ backgroundImage: `url(${backgroundImage})` }}
    >
        <div className="hero-content">
            <h1>{title}</h1>
            <p>{text}</p>
        </div>
    </div>
);

Hero.propTypes = {
    backgroundImage: PropType.string.isRequired,
    title: PropType.string.isRequired,
    text: PropType.string.isRequired,
};

export default Hero;
