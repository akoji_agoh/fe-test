import React from 'react';
import { shallow } from 'enzyme';
import Hero from '../Hero';

describe('Hero component Unit Tests', () => {
    const props = {
        backgroundImage: 'img1.png',
        title: 'Hello there',
        text: 'Some random text'
    };
    let component = null;

    beforeEach(() => {
        component = shallow(<Hero { ...props } />);
    });

    it('should render Hero component', () => {
        expect(component).toHaveLength(1);
        expect(component).toMatchSnapshot();
    });

    it('should display the title prop', () => {
        expect(component.find('h1').html()).toContain(props.title);
    });

    it('should display the text prop', () => {        
        expect(component.find('p').html()).toContain(props.text);
    });
});
