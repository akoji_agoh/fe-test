import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { getStockAvailabilityInfo } from '../../settings';
import { QuantitySelector } from '../';
import BuyButton from '../../assets/img/products/buy_button.svg';

const ProductCard = ({ product, shoppingCart, isInCart }) => (
    <div className="product-card">
        <NavLink to={`/product/${product.category}/${product.key}`}>
            <img src={product.imageUrl} alt={product.title} />
        </NavLink>
        <div className="name">{product.title}</div>
        <div className="price">&pound;{Number(product.price).toFixed(2)}&nbsp;{product.amount}</div>
        {getStockAvailabilityInfo(product) && 
            <div className={`stock-info ${getStockAvailabilityInfo(product).id}`}>{getStockAvailabilityInfo(product).label}</div>
        }
        {isInCart &&
            <QuantitySelector product={product} shoppingCart={shoppingCart} />}
        {!isInCart &&
            <NavLink 
                to={`/product/${product.category}/${product.key}`}
                className="buy-button"
                style={{ backgroundImage: `url(${BuyButton})` }}
            />
        }
    </div>
);

ProductCard.propTypes = {
    product: PropTypes.object.isRequired,
    shoppingCart: PropTypes.object.isRequired,
    isInCart: PropTypes.bool.isRequired,
};

export default ProductCard;
