import React from 'react';
import { shallow } from 'enzyme';
import ProductCard from '../ProductCard';
import { QuantitySelector } from '../../';

describe('ProductCard unit tests', () => {
    const props = {
        product: {
            key: 0,
            title: 'Product 0',
            price: .50,
            amount: 'pint',
            imageUrl: 'product_0.png'
        },
        shoppingCart: {
            items: [],
            orderTotal: 0.00,
            subTotal: 0.00,
            discount: 0.00,
        },
        isInCart: false,
    };

    let component = null;
    beforeEach(() => {
        component = shallow(<ProductCard {...props} />);
    });

    it('should render product card component', () => {
        expect(component).toHaveLength(1);
        expect(component).toMatchSnapshot();
    });

    it('should show product details', () => {
        expect(component.find('.name').html()).toContain(props.product.title);
        expect(component.find('.price').html()).toContain('£' + Number(props.product.price).toFixed(2));
        expect(component.find('.price').html()).toContain(props.product.amount);
    });

    it('should show Buy button if product is not in cart', () => {
        component.setProps({ ...props, isInCart: false });
        expect(component.find('.buy-button')).toHaveLength(1);
        expect(component.contains(<QuantitySelector product={props.product} shoppingCart={props.shoppingCart} />)).toBeFalsy();
    });
    
    it('should show quantity selector if product is in cart', () => {
        component.setProps({ ...props, isInCart: true });
        expect(component.find('.buy-button')).toHaveLength(0);
        expect(component.contains(<QuantitySelector product={props.product} shoppingCart={props.shoppingCart} />)).toBeTruthy();
    });
});
