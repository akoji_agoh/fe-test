import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { BiPlus, BiMinus } from 'react-icons/bi';
import { updateItemQuantity, removeItem } from '../../store/shoppingCart/shoppingCartActions';

const QuantitySelector = ({ product, shoppingCart, updateItemQuantity, removeItem }) => {
    const getCartItemQuantity = () => {
        if (!product || product.stock <= 0) {
            return 1;
        }

        const { items } = shoppingCart.data;
        const itemFound = items.find(item => item.key === product.key) || null;
        if (!itemFound) {
            return 1;
        }
        return itemFound.quantity;
    };

    const isItemInCart = () => {
        const { items } = shoppingCart.data;
        if (items.length <= 0) {
            return false;
        }
        const itemFound = items.find(item => item.key === product.key) || null;
        return itemFound;
    };

    const onUpdateQuantity = (value) => {
        if (!product || product.stock <= 0 || !isItemInCart()) {
            return;
        }
        updateItemQuantity({ item: product, quantity: value });
    };

    const onIncreaseQuantity = () => {
        if (!product || product.stock <= 0 || !isItemInCart() || product.stock <= getCartItemQuantity()) {
            return;
        }
        onUpdateQuantity(getCartItemQuantity() + 1);
    };

    const onDecreaseQuantity = () => {
        if (!product || product.stock <= 0 || !isItemInCart()) {
            return;
        }
        if ((getCartItemQuantity() - 1) > 0) {
            onUpdateQuantity(getCartItemQuantity() - 1);
        } else {
            onRemoveItem();
        }
    };

    const onRemoveItem = () => {
        if (!product || product.stock <= 0 || !isItemInCart()) {
            return;
        }
        removeItem(product);
    };

    return (
        <div className="quantity-wrapper">
            <button
                type="button"
                className="btn decrease"
                onClick={() => onDecreaseQuantity()}
            >
                <BiMinus />
            </button>
            <span className="quantity">{getCartItemQuantity()}</span>
            <button
                type="button"
                className="btn increase"
                disabled={product.stock <= getCartItemQuantity()}
                onClick={() => onIncreaseQuantity()}
            >
                <BiPlus />
            </button>
        </div>
    );
};

QuantitySelector.propTypes = {
    product: PropTypes.object.isRequired,
    shoppingCart: PropTypes.object.isRequired,
    updateItemQuantity: PropTypes.func.isRequired,
    removeItem: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
    return {
        products: state.products,
        shoppingCart: state.shoppingCart,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        updateItemQuantity: params => dispatch(updateItemQuantity(params)),
        removeItem: item => dispatch(removeItem(item))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(QuantitySelector);
