import React from 'react';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import QuantitySelector from '../QuantitySelector';
import { productInitialState } from '../../../store/products/productReducer';
import { shoppingCartInitialState } from '../../../store/shoppingCart/shoppingCartReducer';
import { updateItemQuantity } from '../../../store/shoppingCart/shoppingCartActions';

describe('QuantitySelector component Unit Test', () => {
    const updateItemQuantityCallBack = jest.fn();
    const removeItemCallBack = jest.fn();
    const props = {
        product: {
            key: 1,
            title: 'Product 1',
            price: .50,
            amount: 'pint',
            imageUrl: 'product_1.png',
            quantity: 3,
            stock: 4,
        },
        shoppingCart: {
            data: {
                items: [
                    {
                        key: 1,
                        title: 'Product 1',
                        price: .50,
                        amount: 'pint',
                        imageUrl: 'product_1.png',
                        quantity: 3,
                        stock: 4,
                    }
                ],
                orderTotal: 0.00,
                subTotal: 0.00,
                discount: 0.00,
            },
        },
        updateItemQuantity: updateItemQuantityCallBack,
        removeItem: removeItemCallBack,
    };

    const mockStore = configureStore();
    const store = mockStore({ 
        products: productInitialState, 
        shoppingCart: {
            ...shoppingCartInitialState,
            ...props.shoppingCart
        } 
    });

    let component = null;

    beforeEach(() => {
        component = mount(
            <Provider store={store}>
                <QuantitySelector {...props} />
            </Provider>
        );
    });

    afterEach(() => {
        store.clearActions();
    });

    it('should render quantity selector component', () => {
        expect(component).toHaveLength(1);
        expect(component).toMatchSnapshot();
    });

    it('should render the quantity of the product passed in', () => {
        expect(component.find('.quantity').html()).toContain(props.product.quantity);
    });

    it('should dispatch action to UPDATE_ITEM_QUANTITY on click of the increase button', () => {
        const expectedAction = [
            updateItemQuantity({ item: props.product, quantity: props.product.quantity + 1 })
        ];
        component.find('.increase').props().onClick();
        expect(store.getActions()).toEqual(expectedAction);
    });

    it('should dispatch action to UPDATE_ITEM_QUANTITY on click of the decrease button', () => {
        const expectedAction = [
            updateItemQuantity({ item: props.product, quantity: props.product.quantity - 1 })
        ];
        component.find('.decrease').props().onClick();
        expect(store.getActions()).toEqual(expectedAction);
    });

    it('should disable increase button if stock not available', () => {
        // console.log(component.find('.increase').html());
    });
});
