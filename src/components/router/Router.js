import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { Products, ProductDetail, ShoppingCart } from '../../modules';

const Router = () => {
    return (
        <Switch>
            <Route path="/shopping-cart" component={ShoppingCart} exact />
            <Route path="/product/:category/:product" component={ProductDetail} exact />
            <Route path="/products" component={Products} exact />
            <Route path="*" component={Products} />
        </Switch>
    );
};

export default Router;
