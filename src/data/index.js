import MilkCategory from '../assets/img/categories/milk-category.jpeg';
import WholeMilkSilverTop from '../assets/img/products/Whole-milk-Silver-top.jpeg';
import Butter from '../assets/img/products/butter.jpeg';
import SterilisedMilk from '../assets/img/products/sterilised_milk.jpeg';
import ReusableBottleTops from '../assets/img/products/Reusable_bottle_tops.jpeg';
import StrawberryMilkshake from '../assets/img/products/Strawberry_milkshake.jpeg';
import ChocolateMilkshake from '../assets/img/products/Chocolate_milkshake.jpeg';
import Eggs from '../assets/img/products/eggs.jpeg';

export const getCategoryBanner = (category) => {
    switch (category) {
        case 'Milk': {
            return MilkCategory;
        }
        default:
            return MilkCategory;
    }
};

export const getProductImage = (product) => {
    switch (product) {
        case 'Whole milk (Silver top)': {
            return WholeMilkSilverTop;
        }
        case 'Butter': {
            return Butter;
        }
        case 'Sterilised milk': {
            return SterilisedMilk;
        }
        case 'Reusable bottle tops': {
            return ReusableBottleTops;
        }
        case 'Strawberry milkshake': {
            return StrawberryMilkshake;
        }
        case 'Chocolate milkshake': {
            return ChocolateMilkshake;
        }
        case '6 Free range eggs': {
            return Eggs;
        }
        default:
            return WholeMilkSilverTop;
    }
};
