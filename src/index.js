import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import toastr from 'toastr';
import reportWebVitals from './reportWebVitals';
import App from './App';
import { history } from './components';
import configureStore from './store';
import './assets/styles/styles.scss';

toastr.options = {
  preventDuplicates: true,
  timeOut: 500,
};

const store = configureStore();

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <BrowserRouter history={history}>
        <App />
      </BrowserRouter>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

reportWebVitals();
