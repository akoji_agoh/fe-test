import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getProducts } from '../../store/products/productActions';
import { PROP_TYPES, getStockAvailabilityInfo, isItemInCart } from '../../settings';
import { Container, Row, Col, Spinner, Accordion, Card, Button } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import BuyNowButton from '../../assets/img/products/buy_now_button.svg';
import { addItem } from '../../store/shoppingCart/shoppingCartActions';
import { QuantitySelector } from '../../components';
import { history } from '../../components';

const ProductDetail = ({ match, products, shoppingCart, getProducts, addItem }) => {
    const [pageLoaded, setPageLoaded] = useState(false);
    const [product, setProduct] = useState(null);

    useEffect(() => {
        if (!pageLoaded) {
            setPageLoaded(true);
            if (products.data.length <= 0) {
                getProducts();
            }
        }
    }, [pageLoaded, products, getProducts]);

    useEffect(() => {
        if (!product && products.data.length > 0) {
            if (match && match.params && match.params.category && match.params.product) {
                const { category, product } = match.params;
                const categoryFound = products.data.find(current => current.title === category) || null;
                if (categoryFound) {
                    const productFound = categoryFound.data.find(current => Number(current.key) === Number(product)) || null;
                    setProduct(productFound);
                }
            }
        }
    }, [match, product, setProduct, products]);

    const onAddItemToCart = () => {
        if (!product || product.stock <= 0) {
            return;
        }
        addItem(product);
    };

    return (
        <Container className="product-detail">
            {products.loading &&
                <div className="spinner-wrapper">
                    <Spinner animation="border" role="status">
                        <span className="sr-only">Loading...</span>
                    </Spinner>
                </div>
            }
            {product &&
                <>
                    <Row className="product-image-name">
                        <Col md={5}>
                            <div className="image-wrapper">
                                <div className="back" onClick={() => history.back()}>Back</div>
                                <img src={product.imageUrl} alt={product.title} />
                            </div>
                        </Col>
                        <Col md={7}>
                            <div className="content">
                                <h1>{product.title}</h1>
                                <div className="price">&pound;{Number(product.price).toFixed(2)}&nbsp;{product.amount}</div>
                                {getStockAvailabilityInfo(product) && 
                                    <div className={`stock-info ${getStockAvailabilityInfo(product).id}`}>{getStockAvailabilityInfo(product).label}</div>
                                }
                                {!isItemInCart(product, shoppingCart) &&
                                    <div
                                        className={`buy-now-button ${getStockAvailabilityInfo(product).id}`}
                                        style={{ backgroundImage: `url(${BuyNowButton})` }}
                                        onClick={() => onAddItemToCart()}                                
                                    />
                                }
                                {isItemInCart(product, shoppingCart) &&
                                    <QuantitySelector product={product} shoppingCart={shoppingCart} />
                                }
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col lg={12}>
                        <Accordion defaultActiveKey="0">
                            <Card>
                                <Card.Header>
                                    <Accordion.Toggle as={Button} variant="link" eventKey="0">
                                        <h3>Description</h3>
                                    </Accordion.Toggle>
                                </Card.Header>
                                <Accordion.Collapse eventKey="0">
                                    <Card.Body>
                                        This dairy-free alternative is made with 100% British oats. The oats are milled and processed in a gluten-free facility – which guarantees no cross-contamination with wheat. And the taste is simply delicious. It’s incredibly versatile for smoothies, hot drinks or cooking. And our oat drink comes in return and reuse glass bottles, just like traditional milk.
                                    </Card.Body>
                                </Accordion.Collapse>
                            </Card>
                            <Card>
                                <Card.Header>
                                    <Accordion.Toggle as={Button} variant="link" eventKey="1">
                                        <h3>Ingredients</h3>
                                    </Accordion.Toggle>
                                </Card.Header>
                                <Accordion.Collapse eventKey="1">
                                    <Card.Body>
                                        Water, oats, sunflower oil, salt. No Allergens
                                    </Card.Body>
                                </Accordion.Collapse>
                            </Card>
                            <Card>
                                <Card.Header>
                                    <Accordion.Toggle as={Button} variant="link" eventKey="2">
                                        <h3>Nutritional Information</h3>
                                    </Accordion.Toggle>
                                </Card.Header>
                                <Accordion.Collapse eventKey="2">
                                    <Card.Body>
                                        <table className="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Typical values</th>
                                                    <th>per 100ml serving</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td colSpan="2" className="text-center text-muted">
                                                        No nutritional information found.
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </Card.Body>
                                </Accordion.Collapse>
                            </Card>
                        </Accordion>
                        </Col>
                    </Row>
                </>
            }
            {!product &&
                <div className="no-product-detail">
                    <h1>Product Not Found.</h1>
                    <NavLink to="/products" className="btn btn-default">See our products</NavLink>
                </div>
            }
        </Container>
    );
};

ProductDetail.propTypes = {
    match: PropTypes.any,
    products: PROP_TYPES.products,
    shoppingCart: PROP_TYPES.shoppingCart,
    getProducts: PropTypes.func.isRequired,
    addItem: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
    return {
        products: state.products,
        shoppingCart: state.shoppingCart,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getProducts: () => dispatch(getProducts()),
        addItem: item => dispatch(addItem(item)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetail);
