import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getProducts } from '../../store/products/productActions';
import { isItemInCart } from '../../settings';
import { Container, Row, Col, Spinner } from 'react-bootstrap';
import { Hero, Filter, ProductCard } from '../../components';
import { getCategoryBanner } from '../../data';
import { RiEqualizerLine } from 'react-icons/ri';

const Products = ({ products, shoppingCart, getProducts }) => {
    const [pageLoaded, setPageLoaded] = useState(false);
    const [activeCategories, setActiveCategories] = useState([]);
    const [showFilterPanel, setShowFilterPanel] = useState(false);

    useEffect(() => {
        if (!pageLoaded) {
            setPageLoaded(true);
            if (products.data.length <= 0) {
                getProducts();
            }
        }
    }, [pageLoaded, products, getProducts]);

    const onFilterSelected = (category) => {
        let result = [];

        if (!activeCategories || activeCategories.length <= 0) {
            result.push(category);
        } else {
            // Check if category is already selected.
            const categoryFound = activeCategories.find(current => current.title === category.title);

            if (categoryFound) {
                result = activeCategories.filter(current => current.title !== category.title);
            } else {
                result = [...activeCategories, category];
            }
        }
        setActiveCategories(result);
    };

    const getFilteredProducts = () => {
        let result = [];

        if (!activeCategories || activeCategories.length <= 0) {
            for (let category of products.data) {
                result = [...result, ...category.data];
            }
        } else {
            for (let category of activeCategories) {
                result = [...result, ...category.data];
            }
        }
        return result;
    };

    return (
        <Container className="product-page">
            {products.loading &&
                <div className="spinner-wrapper">
                    <Spinner animation="border" role="status">
                        <span className="sr-only">Loading...</span>
                    </Spinner>
                </div>
            }
            <Row>
                <Col lg={3} className="d-none d-lg-block">
                    <Filter
                        categories={products.data}
                        activeCategories={activeCategories}
                        onSelect={(category) => onFilterSelected(category)}
                        onClearAll={() => setActiveCategories([])} />
                </Col>
                <Col md={12} lg={9}>                    
                    <Hero
                        backgroundImage={getCategoryBanner('Milk')}
                        title={'Milk'}
                        text={'Our cow’s milk is delivered fresh from the dairy in traditional glass bottles, whole, semi-skimmed or skimmed. Try our bottled oat milk, for a dairy-free alternative which tastes great on its own or with tea and coffee.'} />
                    <div className="mobile-filter-toggle">
                        <div className="filter-toggle-wrapper">
                            <div 
                                className={`filter-toggle ${showFilterPanel ? 'active' : ''}`}
                                onClick={() => setShowFilterPanel(!showFilterPanel)}>
                                <span>
                                    Filter&nbsp;
                                    {activeCategories && activeCategories.length > 0 &&
                                        <span>({activeCategories.length})</span>
                                    }
                                </span>
                                <RiEqualizerLine />
                            </div>
                            <div className={`filter-content ${showFilterPanel ? 'show' : ''}`}>
                                <Filter
                                    categories={products.data}
                                    activeCategories={activeCategories}
                                    onSelect={(category) => onFilterSelected(category)}
                                    onClearAll={() => setActiveCategories([])} />
                            </div>
                        </div>
                    </div>
                    <div className="products">
                        {getFilteredProducts().map((product, index) => 
                            <ProductCard
                                key={index}
                                product={product}
                                shoppingCart={shoppingCart}
                                isInCart={isItemInCart(product, shoppingCart)}
                            />
                        )}
                    </div>
                </Col>
            </Row>
        </Container>
    );
};

Products.propTypes = {
    products: PropTypes.object.isRequired,
    shoppingCart: PropTypes.object.isRequired,
    getProducts: PropTypes.func.isRequired
};

const mapStateToProps = (state) => {
    return {
        products: state.products,
        shoppingCart: state.shoppingCart,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getProducts: () => dispatch(getProducts()),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Products);
