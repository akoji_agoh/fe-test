import React from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import PropType from 'prop-types';
import { Button, Col, Container, Row } from 'react-bootstrap';
import { Hero, QuantitySelector } from '../../components';
import { getCategoryBanner } from '../../data';
import { removeItem, clearCart } from '../../store/shoppingCart/shoppingCartActions';
import { FaTimes } from 'react-icons/fa';
import { VAT } from '../../settings';

const ShoppingCart = ({ shoppingCart, removeItem, clearShoppingCart }) => {
    return (
        <Container className="shopping-cart-page">
            <Hero
                backgroundImage={getCategoryBanner('Milk')}
                title={'Shopping Cart'}
                text=""/>
            <Row className="cart-content">
                <Col xs={12}>
                    {(!shoppingCart || !shoppingCart.data || !shoppingCart.data.items || shoppingCart.data.items.length <= 0) &&
                        <h4 className="text-muted text-center cart-empty">Your basket is empty.</h4>
                    }
                    {shoppingCart && shoppingCart.data && shoppingCart.data.items && shoppingCart.data.items.length > 0 &&
                        <>
                            <table className="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th className="c-medium">Quantity</th>
                                        <th className="c-small">Price</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {shoppingCart.data.items.map((item, index) => 
                                        <tr key={index}>
                                            <td>
                                                <NavLink to={`/product/${item.category}/${item.key}`}>
                                                    {item.title}
                                                </NavLink>
                                            </td>
                                            <td className="c-medium">
                                                <div className="quantity-remove">
                                                    <QuantitySelector product={item} shoppingCart={shoppingCart} />
                                                    <FaTimes
                                                        className="remove"
                                                        onClick={() => removeItem(item)}
                                                    />
                                                </div>
                                            </td>
                                            <td className="c-small price">
                                                &pound;{Number(item.price * item.quantity).toFixed(2)}
                                            </td>
                                        </tr>,
                                    )}
                                </tbody>
                            </table>
                            <div
                                className="clear-basket"
                                onClick={() => clearShoppingCart()}
                            >
                                Clear shopping cart
                            </div>
                            <div className="total">
                                <span>
                                    Sub Total: &pound;{Number(shoppingCart.data.subTotal).toFixed(2)}
                                </span>
                            </div>
                            <div className="total">
                                <span>
                                    VAT: &pound;{Number(shoppingCart.data.subTotal * VAT).toFixed(2)}
                                </span>
                            </div>
                            <div className="total">
                                <span>
                                    Order Total: &pound;{Number(shoppingCart.data.orderTotal).toFixed(2)}
                                </span>
                            </div>
                            <Button 
                                variant="primary"
                                className="checkout-btn"
                            >
                                Checkout
                            </Button>
                        </>
                    }
                </Col>
            </Row>
        </Container>
    );
};

ShoppingCart.propTypes = {
    shoppingCart: PropType.object,
    removeItem: PropType.func,
    clearShoppingCart: PropType.func,
};

const mapStateToProps = (state) => {
    return {
        shoppingCart: state.shoppingCart,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        removeItem: item => dispatch(removeItem(item)),
        clearShoppingCart: () => dispatch(clearCart())
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(ShoppingCart);
