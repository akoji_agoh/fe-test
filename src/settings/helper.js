export const getStockAvailabilityInfo = (product) => {
    if (!product) {
        return null;
    }

    const { stock } = product;
    if (stock > 10) {
        return {
            id: 'in-stock',
            label: 'In stock'
        };
    } else if (stock <= 10) {
        return {
            id: 'few-left',
            label: 'Few left'
        };
    }
    return {
        id: 'out-of-stock',
        label: 'Out of stock'
    };
};

export const isItemInCart = (product, shoppingCart) => {
    const { items } = shoppingCart.data;
    if (items.length <= 0) {
        return false;
    }
    const itemFound = items.find(item => item.key === product.key) || null;
    return itemFound ? true : false;
};

export const getTotalItemsInCart = (shoppingCart) => {
    if (!shoppingCart || !shoppingCart.data || !shoppingCart.data.items) {
        return 0;
    }
    return shoppingCart.data.items.reduce((a, b) => {
        return a += b.quantity;
    }, 0);
};
