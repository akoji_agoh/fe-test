import PropTypes from 'prop-types';

const PROP_TYPES = {
    products: PropTypes.shape({
        loading: PropTypes.bool,
        error: PropTypes.any,
        data: PropTypes.array,
    }),
    shoppingCart: PropTypes.shape({
        loading: PropTypes.bool,
        error: PropTypes.any,
        data: PropTypes.object,
    }),
};

export default PROP_TYPES;
