import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import { productReducer as products } from './products/productReducer.js';
import { shoppingCartReducer as shoppingCart } from './shoppingCart/shoppingCartReducer';
import { apiMiddleware } from 'redux-api-middleware';
import thunk from 'redux-thunk';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducer = () => 
    combineReducers({
        products,
        shoppingCart,
    });

export default function configureStore(initialState) {
    return createStore(
        rootReducer(),
        initialState,
        composeEnhancers(
            applyMiddleware(thunk, apiMiddleware)
        )
    );
}
