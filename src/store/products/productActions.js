import { RSAA } from 'redux-api-middleware';

export const GET_PRODUCTS_REQUEST = 'GET_PRODUCTS_REQUEST';
export const GET_PRODUCTS_SUCCESS = 'GET_PRODUCTS_SUCCESS';
export const GET_PRODUCTS_ERROR = 'GET_PRODUCTS_ERROR';

export const getProducts = () => {
    return {
        [RSAA]: {
            endpoint: 'https://developertests.z33.web.core.windows.net/ReactTestData.json',
            method: 'GET',
            types: [
                GET_PRODUCTS_REQUEST,
                GET_PRODUCTS_SUCCESS,
                GET_PRODUCTS_ERROR,
            ],
        },
    };
};
