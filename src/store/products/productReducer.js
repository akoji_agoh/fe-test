import {
    GET_PRODUCTS_REQUEST,
    GET_PRODUCTS_SUCCESS,
    GET_PRODUCTS_ERROR,
} from './productActions';
import { getCategoryBanner, getProductImage } from '../../data';

export const productInitialState = {
    loading: false,
    error: '',
    data: [],
};

export const productReducer = (state = productInitialState, action) => {
    switch(action.type) {
        case GET_PRODUCTS_REQUEST: {
            return {
                ...state,
                loading: true,
            };
        }
        case GET_PRODUCTS_SUCCESS: {
            let categories = action.payload;
            if (categories && categories.length > 0) {
                categories = categories.map(category => {
                    category.imageUrl = getCategoryBanner(category.title);
                    category.data = category.data.map(product => {
                        product.imageUrl = getProductImage(product.title);
                        product.category = category.title;
                        return product;
                    });
                    return category;
                })
            }
            return {
                ...state,
                loading: false,
                data: categories,
            };
        }
        case GET_PRODUCTS_ERROR: {
            return {
                ...state,
                loading: false,
                error: action.payload,
            };
        }
        default:
            return state;
    }
}
