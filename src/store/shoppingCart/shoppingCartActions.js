import toastr from 'toastr';

export const GET_SHOPPING_CART_REQUEST = 'GET_SHOPPING_CART_REQUEST';
export const GET_SHOPPING_CART_SUCCESS = 'GET_SHOPPING_CART_SUCCESS';
export const GET_SHOPPING_CART_ERROR = 'GET_SHOPPING_CART_ERROR';
export const ADD_ITEM_TO_CART = 'ADD_ITEM_TO_CART';
export const UPDATE_ITEM_QUANTITY = 'UPDATE_ITEM_QUANTITY';
export const REMOVE_ITEM_FROM_CART = 'REMOVE_ITEM_FROM_CART';
export const CLEAR_CART = 'CLEAR_CART';

export const addItem = (item) => {
    toastr.success('Item added to cart');
    return {
        type: ADD_ITEM_TO_CART,
        payload: item,
    }
};

export const updateItemQuantity = (params = { item: null, quantity: 0 }) => {
    toastr.success('Item quantity updated');
    return {
        type: UPDATE_ITEM_QUANTITY,
        payload: params,
    }
};

export const removeItem = (item) => {
    // toastr.success('Item removed from cart');
    return {
        type: REMOVE_ITEM_FROM_CART,
        payload: item,
    }
};

export const clearCart = () => {
    toastr.success('Cart cleared.');
    return {
        type: CLEAR_CART,
    }
};
