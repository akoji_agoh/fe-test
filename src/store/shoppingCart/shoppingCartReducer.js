import { VAT } from '../../settings';
import {
    GET_SHOPPING_CART_REQUEST, 
    GET_SHOPPING_CART_SUCCESS,
    GET_SHOPPING_CART_ERROR,
    ADD_ITEM_TO_CART,
    UPDATE_ITEM_QUANTITY,
    REMOVE_ITEM_FROM_CART,
    CLEAR_CART,
} from './shoppingCartActions';

export const shoppingCartInitialState = {
    loading: false,
    error: '',
    data: {
        items: [],
        orderTotal: 0.00,
        subTotal: 0.00,
        discount: 0.00,
    },
};

export const shoppingCartReducer = (state = shoppingCartInitialState, action) => {
    switch(action.type) {
        case GET_SHOPPING_CART_REQUEST: {
            return {
                ...state,
                loading: true,
            }
        }
        case GET_SHOPPING_CART_SUCCESS: {
            return {
                ...state,
                loading: false,
                data: action.payload
            }
        }
        case GET_SHOPPING_CART_ERROR: {
            return {
                ...state,
                loading: false,
                data: action.payload
            }
        }
        case ADD_ITEM_TO_CART: {
            const { items } = state.data;
            let updatedItems = [...items];
            if (updatedItems.length <= 0) {
                updatedItems.push({
                    ...action.payload,
                    quantity: 1,
                });
            } else {
                // Check if item is already in cart.
                const itemFound = updatedItems.find(item => item.key === action.payload.key) || null;
                if (!itemFound) {
                    updatedItems.push({
                        ...action.payload,
                        quantity: 1,
                    });
                }
            }

            const total = updatedItems.reduce((a, b) => {
                return a += b.price * b.quantity;
            }, 0);

            return {
                ...state,
                data: {
                    ...state.data,
                    items: updatedItems,
                    subTotal: total,
                    orderTotal: total + (total * VAT),
                },
            };
        }
        case UPDATE_ITEM_QUANTITY: {
            const { items } = state.data;
            let updatedItems = [...items];
            if (updatedItems.length > 0) {
                updatedItems = updatedItems.map(item => {
                    if (item.key === action.payload.item.key) {
                        item.quantity = action.payload.quantity;
                    }
                    return item;
                });
            }
            const total = updatedItems.reduce((a, b) => {
                return a += b.price * b.quantity;
            }, 0);
            return {
                ...state,
                data: {
                    ...state.data,
                    items: updatedItems,
                    subTotal: total,
                    orderTotal: total + (total * VAT),
                },
            };
        }
        case REMOVE_ITEM_FROM_CART: {
            const { items } = state.data;
            let updatedItems = [...items];
            if (updatedItems.length > 0) {
                updatedItems = updatedItems.filter(item => item.key !== action.payload.key);
            }
            const total = updatedItems.reduce((a, b) => {
                return a += b.price * b.quantity;
            }, 0);
            return {
                ...state,
                data: {
                    ...state.data,
                    items: updatedItems,
                    subTotal: total,
                    orderTotal: total + (total * VAT),
                },
            };
        }
        case CLEAR_CART: {
            return {
                ...shoppingCartInitialState,
            };
        }
        default:
            return state;
    }
};
